package com.capg.training.service;

import java.util.List;

import com.capg.training.beans.Trainee;
import com.capg.training.dao.TraineeDAO;

public class TraineeService {
	
	TraineeDAO dao= new TraineeDAO();
	public void addTrainee(Trainee t) {
		dao.addTrainee(t);
	}
	
	public List<Trainee> getTrainees(){
		return dao.getTrainees();
	}

}
